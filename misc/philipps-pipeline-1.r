library(data.table)
library(randomForest)
library(RColorBrewer)

### define input files

CN_BED <- snakemake@input[["cn"]]
METH_RDATA <- snakemake@input[["meth"]]
TMP <- snakemake@output[["tmp"]]

### make arm level CN profile

system(paste("tail -n +2 ",CN_BED," | /export/dataBIOINFO/opt/BIOINFO/tools/bedtools-2.17.0/bin/bedtools intersect -a - -b /lnec/SANSON/nanopore/pipeline/data/cytoBand.txt -wb > ", TMP, sep=""))

CN <- fread(TMP)
CN$chrArm <- paste("chr",CN$V1,substr(CN$V10,1,1),sep="")

aggCN <- CN[,weighted.mean(V5,V3 - V2),by=.(chrArm)]

aggCN$CN <- as.integer(cut(aggCN$V1,c(-1000,-0.4,0.3,1000), label=c(-1,0,1)))
aggCN$V1 <- NULL
rownames(aggCN) <- aggCN$chrArm
aggCN$chrArm <- NULL

tAggCN <- t(as.matrix(aggCN))
colnames(tAggCN) <- rownames(aggCN)

### merge with methylation calls

load(METH_RDATA)

case <- data.frame(t(sapply(case, function(x) as.numeric(as.character(x)))))

case_CN_Meth <- cbind(case,data.frame(tAggCN))

### load CN training set

load(snakemake@input[["trainingset_cn"]])

# remove germline samples
cn_matrix <- subset(cn_matrix, grepl("TCGA-\\w+{2}-\\w+{4}-01",V6))

### generate methylation training set

library(rhdf5)
fh5 = snakemake@input[["trainingset_meth"]]

# dump HDF5 training set content
h5ls(fh5)

Dx <- as.factor(h5read(fh5,"Dx"))
sampleIDs <- h5read(fh5,"sampleIDs")
trainingProbes <- h5read(fh5,"probeIDs")

probes <- intersect(colnames(case_CN_Meth), trainingProbes)
idxs <- match(probes, trainingProbes)

# use threshold > 0.6
panbin <- data.frame(Dx, (as.matrix(h5read(fh5, "betaValues", index=list(NULL, idxs))) > 0.1) * 1)

colnames(panbin) <- c("Dx", trainingProbes[idxs])

# restrict to primary tumors (sample code 01)
panbin$sampleID <- gsub("\\.","-",sampleIDs)
panbin <- subset(panbin, grepl("TCGA-\\w+-\\w+-01", sampleID) | !grepl("TCGA", sampleID))

# remove samples without diagnosis
# panbin <- subset(panbin, Dx != "NA" & !grepl("MB|K27|G34", Dx))
panbin <- subset(panbin, Dx != "NA")
panbin$Dx <- droplevels(panbin$Dx)

panbin$patientID <- sub("(TCGA-\\w+-\\w+)-.*","\\1", panbin$sampleID)
panbin$sampleID <- NULL


### create sample set for CN, methylation or both depending on parameter

# CN + methylation
CNmeth <- merge(cn_matrix[,-2:-1],panbin,by="patientID")

# CN only
if (snakemake@wildcards[["modality"]] == "CN")
  CNmeth <- CNmeth[,grepl("chr|Dx",colnames(CNmeth))]

# methylation only
if (snakemake@wildcards[["modality"]] == "meth")
  CNmeth <- panbin

### classify

#### do not discriminate 1p/19q status
CNmeth$Dx <- sub("-non|-cod","",CNmeth$Dx)

cols <- intersect(colnames(CNmeth),colnames(case_CN_Meth))
rf <- randomForest(as.factor(Dx) ~., CNmeth[,c("Dx",cols)], ntree=500, do.trace=10)

print(rf)

votes <- predict(rf, case_CN_Meth[,cols], type="vote")

rownames(votes) <- snakemake@wildcards[["sample"]]
save(votes, file=snakemake@output[["votes"]])

pdf(snakemake@output[["pdf"]])
pie(votes[1,], labels = paste(colnames(votes)," (",as.integer(votes[1,]*100)," %)",sep=""), main = snakemake@wildcards[["sample"]], col=brewer.pal(ncol(votes),"Set1"))
dev.off()
