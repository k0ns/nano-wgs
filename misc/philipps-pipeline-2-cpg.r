
cpg <- read.table(snakemake@input[[1]], sep="\t", header=T)

probes450K <- read.table("/lnec/SANSON/nanopore/pipeline/data/450K_hg19.bed", sep = "\t", header = F)
colnames(probes450K) <- c("chromosome","start","end","probeID","X","XX")

overlap <- merge(cpg, probes450K, by=c("chromosome","start"))

# take called singleton CpG sites
CpGcalls <- subset(overlap, num_cpgs_in_group == 1 & !(chromosome %in% c("chrX","chrY")))

CpGcalls <- CpGcalls[!duplicated(CpGcalls$probeID),]

CpGcalls$isMethylated <- (CpGcalls$methylated_frequency > 0.5) * 1
rownames(CpGcalls) <- CpGcalls$probeID

case <- data.frame(t(CpGcalls))
case <- case["isMethylated",]

save(case, file = snakemake@output[[1]])




