#!/bin/bash
/fast/users/helmsauk_c/scratch/miniconda/bin/read_fast5_basecaller.py --flowcell FLO-MIN106 --kit SQK-RAD003 --output_format fastq --input /fast/users/helmsauk_c/scratch/nano-wgs/debugging-data/ --recursive --disable-pings --save_path /fast/users/helmsauk_c/scratch/nano-wgs/8734T-basecalled-debug/ --worker_threads 4

